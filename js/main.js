/**
 * 
 *  Open index.html
 *  ASDW controls small box
 *  zoom with +/- 
 *  view resizes with window size
 *  
 */

// Matterjs objects
Matter.use('matter-collision-events');
this.World = Matter.World;
this.Body = Matter.Body;
this.Bodies = Matter.Bodies;
this.Engine = Matter.Engine;
this.Events = Matter.Events;
this.Render = Matter.Render;
this.Query = Matter.Query;
this.Bounds = Matter.Bounds;
this.Vector = Matter.Vector;
this.Mouse = Matter.Mouse;
this.MouseConstraint = Matter.MouseConstraint;

// dynamic based on window size
//this.client_width   = 800; 
//this.client_height  = 600;

this.world_bound_X = 9999999;
this.world_bound_Y = 9999999;
this.zoom = 1;
this.bounds_scale_target = {};
var menu = new Audio('./sounds/menu.mp3');
var music = new Audio('./sounds/background.mp3');
var jump = new Audio('./sounds/jump.mp3');
var death = new Audio('./sounds/death.mp3');
var featherOn = new Audio('./sounds/featherOn.mp3');
var featherOff = new Audio('./sounds/featherOff.mp3');
var immortalOn = new Audio('./sounds/immortalOn.mp3');
var immortalOff = new Audio('./sounds/immortalOff.mp3');
var speedOn = new Audio('./sounds/speedOn.mp3');
var speedOff = new Audio('./sounds/speedOff.mp3');

if (localStorage.getItem('muted') == 'no') {
    menu.volume = 0.05;
    music.volume = 0.05;
    jump.volume = 0.1;
    death.volume = 0.25;
    immortalOff.volume = 0.25;
    immortalOn.volume = 0.25;
    speedOn.volume = 0.25;
    speedOff.volume = 0.25;
    featherOn.volume = 0.25;
    featherOff.volume = 0.25;
} else {
    menu.volume = 0;
    music.volume = 0;
    jump.volume = 0;
    death.volume = 0;
    immortalOff.volume = 0.00;
    immortalOn.volume = 0.00;
    speedOn.volume = 0.00;
    speedOff.volume = 0.00;
    featherOn.volume = 0.00;
    featherOff.volume = 0.00;
}
death.currentTime = 0.3;
music.loop = true;



var jumpedCount = 0;
//score / Powerup Elements
let $currentScore = $('.current-score span');
let currentScore = 0;

let $powerUp = $('.powerup');
let scoreAtPowerUp;

let $mute = $('.mute');
let inMenu = true;

let powerupSpeed = false;
let powerupFeather = false;
let powerupImmortal = false;


let playerAlive = true;
let alertDeadCounter = 0;

let $highScore = $('.high-score');
let lastScore = 0;
let highScore = 0;



// create an engine
this.engine = this.Engine.create();
this.world = this.engine.world;

this.canvas_element = document.getElementById('gamefield');
// create a renderer
this.render = this.Render.create({
    canvas: this.canvas_element,
    engine: this.engine,
    options: {
        width: 1280,
        height: 720,
        wireframes: false,
        hasBounds: true,
        background: 'https://i.ytimg.com/vi/0hR3tEHMS1o/maxresdefault.jpg'
    }
});

// random value
function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}


// Create a player
function createPlayer() {
    var player = this.Bodies.circle(0, this.world_bound_Y - 10, 25.6, {
        airFriction: 0.0005, render: {
            sprite: {
                texture: './img/player_stretched.png',
                xScale: 0.20,
                yScale: 0.20
            }
        }
    });
    return player;
}

let bodiesToDespawn = [];
// Create Bodies for World

function addEnemies(arr, minEnemyX1, maxEnemyX1) {
    var minEnemyX = minEnemyX1;
    var maxEnemyX = maxEnemyX1;
    for (let i = 0; i < 250; i++) {
        var enemySize = randomIntFromInterval(40, 130);
        arr.push(this.Bodies.trapezoid(randomIntFromInterval(minEnemyX, maxEnemyX), this.world_bound_Y - 400, enemySize, enemySize, 1, {
            label: 'ENEMY',
        }));

        minEnemyX += 850;
        maxEnemyX += 900;
    }
}

function addPowerUps(arr, minPowerUpX, maxPowerUpX) {
    var minPowerUpX = minPowerUpX;
    var maxPowerUpX = maxPowerUpX;


    arr.push(this.Bodies.rectangle(randomIntFromInterval(minPowerUpX, maxPowerUpX), this.world_bound_Y - 130, 55.68, 53.22, {
        label: 'STAR',
        isStatic: true,
        render: {
            sprite: {
                texture: 'https://i.ya-webdesign.com/images/mario-star-png-1.png',
                xScale: 0.03,
                yScale: 0.03
            }
        }
    }));

    for (let i = 0; i < 2; i++) {
        minPowerUpX += 15000
        maxPowerUpX += 30000;

        arr.push(this.Bodies.rectangle(randomIntFromInterval(minPowerUpX, maxPowerUpX), this.world_bound_Y - 400, 65.28, 39.47, {
            label: 'SPEED',
            render: {
                sprite: {
                    texture: 'https://i.ya-webdesign.com/images/cartoon-yeezy-png-3.png',
                    xScale: 0.12,
                    yScale: 0.12
                }
            }
        }));

        arr.push(this.Bodies.rectangle(randomIntFromInterval(minPowerUpX, maxPowerUpX), this.world_bound_Y - 400, 61.817, 30.617, {
            label: 'FEATHER',
            render: {
                sprite: {
                    texture: './img/feather.png',
                    xScale: 0.14,
                    yScale: 0.14
                }
            }
        }));
    }
}

function addBodies(arr) {
    addEnemies(arr, 1500, 1900);
    addPowerUps(arr, 10000, 40000);

    player = createPlayer();
    arr.push(player);
    bodies.push(this.Bodies.rectangle(0, this.world_bound_Y - 1, 1000000, 20, {
        isStatic: true,
        label: 'GROUND'
    }));
}
var bodies = [];
addBodies(bodies)
// add all of the bodies to the world
this.World.add(this.engine.world, bodies, [menu.loop = true, menu.play()]);

// run the engine
this.Engine.run(this.engine);
// run the renderer
this.Render.run(this.render);
// make the world bounds a little bigger than the render bounds
this.world_padding = 9999999;
this.engine.world.bounds.min.x = 0 - this.world_padding;
this.engine.world.bounds.min.y = 0 - this.world_padding;
this.engine.world.bounds.max.x = this.world_bound_X + this.world_padding;
this.engine.world.bounds.max.y = this.world_bound_Y;
this.bounds_scale_target = 1;
this.bounds_scale = { x: 1, y: 1 };

if (localStorage.getItem('enisDashLastScore')) {
    $highScore.text('Highscore: ' + localStorage.getItem('enisDashLastScore'));
    highScore = localStorage.getItem('enisDashLastScore')
} else {
    $highScore.text('Highscore: 0');
    highScore = 0;
}


player.onCollide(function (pair) {
    if (pair.bodyA.label == 'ENEMY' || pair.bodyB.label == 'ENEMY') {
        music.pause();
        music.currentTime = 0;
        death.play();
        playerAlive = false;
        if (alertDeadCounter == 0) {
            alertDeadCounter++;

            lastScore = Math.round(currentScore / 35)
            if (lastScore > localStorage.getItem('enisDashLastScore')) {
                localStorage.setItem('enisDashLastScore', lastScore);
                $highScore.text('Highscore: ' + localStorage.getItem('enisDashLastScore'));
            }

            Swal.fire({
                title: 'You died',
                text: 'you reached ' + Math.round(currentScore / 35) + ' points, better luck next time!',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Wanna try again?'
            }).then((result) => {
                location.reload();
            })
            $powerUp.text('');
        }
    }
    if (pair.bodyB.label == 'GROUND') {
        that.jumpedCount = 0;

        if (powerupSpeed) {
            if (currentScore >= scoreAtPowerUp + 4000) {
                $powerUp.text('');
                powerupSpeed = false;
                speedOff.play();
            }
        } else if (powerupFeather) {
            if (currentScore >= scoreAtPowerUp + 3000) {
                $powerUp.text('');
                powerupFeather = false;
                featherOff.play();
            }
        } else if (powerupImmortal) {
            if (currentScore >= scoreAtPowerUp + 700) {
                $powerUp.text('');
                powerupImmortal = false;
                immortalOff.play();
            }
        }
    }
    if (pair.bodyA.label == 'STAR' || pair.bodyB.label == 'STAR') {
        immortalOn.play();
        setPowerUpScore();
        powerupImmortal = true;
        $powerUp.text('Immortal');
        for (let i = 0; i < 251; i++) {
            that.World.remove(that.engine.world, that.bodies[i]);
        }
        var enemies = [];
        addEnemies(enemies, that.player.position.x + 9000, that.player.position.x + 9400);
        that.World.add(that.engine.world, enemies);
    }

    if (pair.bodyA.label == 'SPEED' || pair.bodyB.label == 'SPEED') {
        speedOn.play();
        setPowerUpScore();
        powerupSpeed = true;
        $powerUp.text('Speed activated');
        that.World.remove(that.engine.world, pair.bodyA);
    }

    if (pair.bodyA.label == 'FEATHER' || pair.bodyB.label == 'FEATHER') {
        featherOn.play();
        setPowerUpScore();
        powerupFeather = true;
        $powerUp.text('Feather activated');
        that.World.remove(that.engine.world, pair.bodyA);
    }
});

this.Events.on(this.engine, 'beforeTick', function () {

    // apply zoom
    var canvas = document.getElementById('gamefield');
    var ctx = canvas.getContext("2d");
    ctx.translate(1280 / 2, 720 / 2);
    ctx.scale(this.zoom, this.zoom);
    ctx.translate(-1280 / 2, -720 / 2);

    // center view at player 
    this.Bounds.shift(this.render.bounds,
        {
            x: player.position.x - 1280 / 2,
            y: player.position.y - 720 / 2
        });

}.bind(this));

window.onresize = function () {
    this.render.canvas.width = 1280;
    this.render.canvas.height = 720;

}.bind(this);

var that = this;

wait = (seconds) =>
    new Promise(resolve =>
        setTimeout(() => resolve(true), seconds)
    );

run = async () => {
    if (await this.wait(1)) {
        if (document.hasFocus()) {
            if (playerAlive) {
                if (powerupSpeed == false) {
                    that.Body.setVelocity(player, { x: 7.5, y: player.velocity.y });
                } else {
                    currentScore++;
                    currentScore++;
                    currentScore++;
                    that.Body.setVelocity(player, { x: 10, y: player.velocity.y });
                }
                currentScore++;
                $currentScore.text(Math.round(currentScore / 35));

                if (Math.round(currentScore / 35) > highScore) {
                    $highScore.text('Highscore: ' + Math.round(currentScore / 35));
                }

            } else {
                that.Body.setVelocity(player, { x: 0, y: player.velocity.y });
            }
        }
        run();
    }
}

function startGame() {
    document.querySelector('.btn').style.visibility = 'hidden';
    document.querySelector('.welcome-text').style.visibility = 'hidden';
    document.querySelector('.score').style.visibility = 'visible';
    inMenu = false;
    menu.volume = 0;

    music.play();
    if (localStorage.getItem('muted') == 'no') {
        music.currentTime = 0;
        music.volume = 0.05;
    }
    run();
}

function mute() {
    if (localStorage.getItem('muted')) {
        if (localStorage.getItem('muted') == 'no') {
            localStorage.setItem('muted', 'yes');
            music.volume = 0.00;
            jump.volume = 0.00;
            death.volume = 0.00;
            menu.volume = 0.00;
            immortalOff.volume = 0.00;
            immortalOn.volume = 0.00;
            speedOn.volume = 0.00;
            speedOff.volume = 0.00;
            featherOn.volume = 0.00;
            featherOff.volume = 0.00;
        } else if (localStorage.getItem('muted') == 'yes') {
            localStorage.setItem('muted', 'no');
            if (inMenu) {
                menu.volume = 0.05;
            } else {
                music.volume = 0.05;
            }
            jump.volume = 0.1;
            death.volume = 0.25;
            immortalOff.volume = 0.25;
            immortalOn.volume = 0.25;
            speedOn.volume = 0.25;
            speedOff.volume = 0.25;
            featherOn.volume = 0.25;
            featherOff.volume = 0.25;
        }
    } else {
        localStorage.setItem('muted', 'yes');
        menu.volume = 0.00;
        music.volume = 0.00;
        jump.volume = 0.00;
        immortalOff.volume = 0.00;
        immortalOn.volume = 0.00;
        speedOn.volume = 0.00;
        speedOff.volume = 0.00;
        featherOn.volume = 0.00;
        featherOff.volume = 0.00;
        death.volume = 0.00;
    }
}

document.querySelector('#btn-start').addEventListener('click', startGame);
document.querySelector('#btn-mute').addEventListener('click', mute);


function setPowerUpScore() {
    scoreAtPowerUp = currentScore;
}

$(document).keydown(function (event) {
    // move the player
    if (event.keyCode == 32) {
        if (that.jumpedCount <= 1) {
            jump.pause();
            jump.currentTime = 0;
            jump.play();
            that.jumpedCount++;
            if (powerupFeather) {
                that.Body.setVelocity(player, { x: player.velocity.x, y: -14 });
            } else {
                that.Body.setVelocity(player, { x: player.velocity.x, y: -10 });
            }
        }
    }
});
